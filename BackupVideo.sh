#!/usr/bin/env bash

youtube-dl --write-info-json \
	   --sub-lang en,fr --write-auto-sub \
	   -o "%(upload_date)s-%(id)s".mkv $1

